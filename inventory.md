## Inventory {#inventory}

### Dance notations {#dance-notations}

Besides the two schools of Laban and Benesh, we can mention other notation systems.

* [Eshkol-Wachman movement notation](https://en.wikipedia.org/wiki/Eshkol-Wachman_movement_notation )
![](https://upload.wikimedia.org/wikipedia/en/d/d1/Manuscript_page.png)

* [Le Danse.R, Raphael Bach](http://notadanse.asso-web.com/uploaded/Notices/notice-du-danse-r.pdf)

### Tools, dictionaries, programming languages {#tools-languages}

* [Understanding Bezier curves](https://en.wikipedia.org/wiki/B%C3%A9zier_curve)
![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/B%C3%A9zier_2_big.gif/240px-B%C3%A9zier_2_big.gif)
![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/B%C3%A9zier_3_big.gif/240px-B%C3%A9zier_3_big.gif)

* Logo language
Seymour Papert, [*Talking Turtle*](https://youtu.be/bOf4EMN6-XA?list=PLlUr-fJyB7_jbEOS0sgQ9HutBN3LUJLa7), video documentary
[Logo commands](http://derrel.net/ep/logo/logo_com.htm )
[Logo Interpreter PZI](https://gitlab.com/pzi-prototyping/seymour)
* [Another Logo Interpreter]https://github.com/inexorabletash/jslogo

* HPGL [HPGL Manuals](http://music.columbia.edu/cmc/chiplotle/manual/chapters/tutorial/shapes.html/ )

HPGL Command → Meaning

```
IN →initialize, start a plotting job
IP → set the initial point (origin), in this case the default 0,0
SC0,40,0,40  → allows scaling in millimeters since 1 mm = 40 plotter units. Each   user-unit is 1 millimeter, in both X and Y directions
SP1 → select pen 1
PU0,0 → lift Pen Up and move to starting point for next action
PD100,0,100,100,0,100,0,0 →put Pen Down and move to the following locations (draw a box around the page)
PU50,50 →Pen  Up and move to X,Y coordinates 50,50 (in this case mm, after the SC  command)
CI25 → draw a circle with radius 25 (mm)
SS → select the standard  font
DT*,1 →set the text delimiter to the asterisk, and do not print them  (the 1, meaning "true")
PU20,80 → lift the pen and move to 20,80
LBHello  World* → draw a label
LTlinetype,length → set line type and its repetition  length
CSxx → set character set (e.g. 33 is German)
DIx,y → set direction of  text given as the catheti
SIww,hh →set character width and height
```



* Metafont

* Hektor
![](http://etat.augmenter-partager.ensad.fr/content/01-home/03-jurg_lehni/10-script.png)

* INkML
[InkML](https://www.w3.org/TR/InkML/ )
![](https://www.w3.org/TR/InkML/hello.png)

* GML
[Graffiti Markup Language - GML](http://www.graffitimarkuplanguage.com/g-m-l-spec/ )


## Mediagraphy, resources {#resources}


### literature {#literature}

* Fernand Baudin, L’écriture au tableau noir, Retz, 1984
* [Walter Crane, Line and Form, 1900](http://www.gutenberg.org/files/25290/25290-h/25290-h.htm )
* Tim Ingold, Une Brève histoire des lignes, Zones Sensibles, 2011
* [Vilém Flusser, «Le geste d’écrire», in Flusser Studies, n°8, Mai 2009](http://reader.lgru.net/texts/le-geste-decrire/ ) - http://www.flusserstudies.net/pag/08/le-geste-d-ecrire.pdf (english link is broken)
* [Donald Knuth, «The Concept of a Meta-Font», in Visible Language, issue 16.1, January 1982; traduction «Le concept de métafonte», in Communication et langages, n°55, 1983, pp. 40–53,](http://www.persee.fr/web/revues/home/prescript/article/colan_0336-1500_1983_num_55_1_1549 )
* Donald Knuth, «Lessons learned from MetaFont», in Visible Language, issue 19.1, December 1985
* Gerrit Noordzij, The Stroke — theory of writing, Hyphen Press, 2006
* [Femke Snelting, «Scenes of Pressures and relief», 2009](http://snelting.domainepublic.net/texts/pressure.txt )
* [Seymour Papert, «Talking Turtle», documentaire vidéo](https://youtu.be/bOf4EMN6-XA?list=PLlUr-fJyB7_jbEOS0sgQ9HutBN3LUJLa7 )
* [François Chastanet, «Dishu: Ground Calligraphy in China»](http://vimeo.com/album/1877116 )
* [Typographic vocabulary relating to body](http://www.zigzaganimal.be/elements/zigzag_anatomie-typographique.pdf )
* [Of the Just Shaping of Letters by Albrecht Dürer](http://www.zigzaganimal.be/elements/just_shaping_scan.pdf ), traduction anglaise à partir de la version latine de Underweysung der Messung mit dem Zirckel und Richtscheyt, livre III



### research {#research}

* [Possible bodies] http://possiblebodies.constantvzw.org/inventory/


### dance, draw {#dance-draw}
* [Drumming, Anne Teresa De Keersmaeker / Rosas & Ictus] http://www.rosas.be/fr/productions/355-drumming
* [Trisha Brown Drawing/Performance](https://www.youtube.com/watch?v=U7DQVW6qRq8)
* [Penwald: 8: 12 by 12 on knees, Tony Orrico](https://www.youtube.com/watch?v=2UeuL5BUgBM)


### write {#write}

* [The Powers of Katsu](https://www.youtube.com/watch?v=6UK6_cp5Q6I)

same movement/sign with differents parts of the body

* [L.A.S.E.R. Tag from Graffiti Research Lab](https://www.youtube.com/watch?v=cY6vfJAxtH8#!)


### measure {#measure}

* Stanley Brouwn.

In 1960, Stanley Brouwn started *This Way Brouwn* serie, sketches of itinerary sketched by passers-by to whom he asks his way and where he then imposes his stamp.
![](https://walker-col.imgix.net/wac_7776.tif?fm=jpg&w=1440&h=1050&fit=max)


* Esther Ferrer

[Le chemin se fait en marchant](https://www.youtube.com/watch?v=S6KflUAhQXI)
