## Process path 
The research has developed itself in stages through several workshops over the the past two years. Reworking a workshop OSP organized to explore performative typography with graphic design students, OSP invited Adva Zakai as a professional performance artist, close to us.

#### 2015 - Workshop Up Pen Down at la Balsamine theater during the “Quinzaine numérique” {#up-pen-down-2015}

<figure>
<img src="http://www.ludi.be/up-pen-down/geometrie-hors-des-sentiers-battus-p42.jpg" />
<img src="
http://ospublish.constantvzw.org/images/var/albums/Up-pen-down--01/11990592_1061281300551391_2757248643048128566_n%20%281%29.jpg" />

<img src="
http://ospublish.constantvzw.org/images/var/albums/Up-pen-down--01/2015-10-25%2016_52_55.jpg?m=1492279203" />


<img src="
http://ospublish.constantvzw.org/images/var/albums/Up-pen-down--01/2015-10-25%2016_44_47.jpg?m=1492279209" />


<img src="http://ospublish.constantvzw.org/images/var/albums/Up-pen-down--01/2015-10-25%2017_34_20.jpg?m=1448905825" />


<figcaption><p>Example of logo drawings code. Human grid as an excersice for collective drawing. Or the mapping of a path.</p></figcaption></figure>


For this 2 day workshop OSP invited artist / dancer / choreographer Adva Zakai to reinterpret a workshop originially organised at xxxx Valence. And took as a starting point the relation between typographic movement and code. We started by executing simple commands or computer code ― walking on the lines a computer would have drawn. We quickly tried to develop movements through more abstract ways. In a exercice, movement rules were regulating the move of a groupe of people, in other each participant was inventing its own movement rules that other participants had to decode. Rapidly we developed a new game of coding and decoding.


#### 2016 - Workshop Metahoguet at La Maison du Livre in the frame of the Saison numériques.{#metahoguet-2015}

<figure>
<img src="https://cloud.osp.kitchen/s/ZNGNY9mJ0RoxKrd/download" />
<img src="https://cloud.osp.kitchen/s/zYnjaGwfgrEjkDt/download" />
<img src="https://cloud.osp.kitchen/s/PspnyW0wlyujLNP/download" />
<img src="https://cloud.osp.kitchen/s/OsrHwMWDRDblUkZ/download" />
<img src="https://cloud.osp.kitchen/s/n1NpaLKyKaJAvrn/download" />
<img src="https://cloud.osp.kitchen/s/vqWaWKZi37uusSs/download" />
<img src="https://cloud.osp.kitchen/s/oWkTjAW2m8xJm9a/download" />
<figcaption><p></p></figcaption></figure>

Hosted by [La Maison du Livre a digital](http://www.lamaisondulivre.be/) a typeface was drawn collaboratively using Metapost in a two day workshop. Metapost is a specialized programming language developed for parametric drawing. As letters were drawn using code rather than manipulating a preview through a graphical interface we could draw together live and simultaneously through a collaborative text editor, Etherpad. During the workshop this shared pad was projected in parallel with its graphical interpretation. This way all the participants could contribute with their letter(s) being aware of the evolution of the letters drawn by the others.

Linked up with this common screen; a *plotter* (pen tracer) allowing live drawing of the designed steps. These snapshots where exhibited so that visitors could feel the collective font process. Next to these snapshots specimens of the final font were drawn with a mural plotter during the opening of the exhibition.

Drawing collaboratively through code asked for a different process in considering shape.


#### 2016 - Worksession at Buda (Courtrai), at OSP WTC and GC Ten Weyngaert (Forest) {#buda-2016}

In order to analyze and further the research started at Balsamine we organized three working sessions.

In the two first sessions in Buda (October 2016) and at WTC (January 2017), we looked back on the experiment at la Balsamine theater. Throughout our discussions we developped a common vocabulary and tried to precise our research scope. We identified three programming languages, each being able to describe or encode a drawing but all coming with their own approaches on how to desribe shape and motion. These languages were linked to approaches of movement we had developed to arrive at strategies of interpretation.

During the last session at GC Ten Weyngaert (January 2017) we invited two external guests, Louise Baduel and Sarah van Lamsweerde, to reflect on the work we'd developed and to find new insigts and directions to continue This last session helped us to realise how each worksession was preceded by exchanges on practices of the participants and that these sessions are crucial to establish a shared vocabulary and shared base of knowledge. The "lecture performance" we're organizing in Balsamine in October 2017 is for us a logical next step.

<figure>
<img src="https://cloud.osp.kitchen/s/z17JEW0gdceZLA8/download" />
<img src="https://cloud.osp.kitchen/s/917rXUNJ3YhGDqo/download" />
<img src="https://cloud.osp.kitchen/s/w53ISinor6Pl6Tz/download" />
<img src="https://cloud.osp.kitchen/s/gvAgxcwudNfmOUc/download" />
<figcaption><p>Session at Buda Kortrijk, with 3 Recipes for an 'R', drawn by the plotter, performed and exploration of the Bezier curve.</p></figcaption>
</figure>
<figure>
<img src="https://cloud.osp.kitchen/s/ITgM0mQJZAH1IEP/download" />
<img src="https://cloud.osp.kitchen/s/ux8sw1mhXtWaisC/download" />
<img src="https://cloud.osp.kitchen/s/RGSw6burIZYWD5w/download" style="clear: both;"/>
<img src="https://cloud.osp.kitchen/s/aAL0eGUXiyIvkiX/download" />
<figcaption><p>Session at WTC, excecuting the recipes in a grid.</p></figcaption>
</figure>
<figure>
<img src="https://cloud.osp.kitchen/s/L2U2833aDlyZZ8p/download" />
<img src="https://cloud.osp.kitchen/s/q0uYlm28kYlKSJr/download" />
<figcaption><p>Research session at GC Ten Weyngaret, with Sarah van Lamsweerde and Lousie Baduel.</p></figcaption>
</figure>
