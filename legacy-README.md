# UP PEN DOWN
### [Théâtre la Balsamine (9-10-2017 → 13-10-2017)](http://balsamine.be/saison-16-17/up-pen-down.html)

* [Process](#process)  
* [Up pen down 2015](#up-pen-down-2015)  
* [Metahoguet 2015](#metahoguet-2015)  
* [Buda 2016](#buda-2016)  
* [Inventory](#inventory)  
* [Dance notations](#dance-notations)  
* [Tools languages](#tools-languages)  
* [Resources](#resources)  
* [Literature](#literature)  
* [Research](#research)  
* [Dance draw](#dance-draw)  
* [Write](#write)  
* [Measure](#measure)


---

Up Pen Down is a research based on the encounter between digital typography and
choreography. By transforming the code into physical movement the project traces
the borders between the practice of choreography and programming. From 9 to 14 of October, in the frame of Saisons numérique, the research will take the shape of a commented performance. After five days
of preparation, a print party will happen on Saturday 14 at 8pm.

---

Up Pen Down est une recherche basée sur la rencontre entre typographie digitale
et chorégraphie. Par un processus physique de mise en mouvement du code le
projet trace des ligatures entre partitions chorégraphiques et programmation.
Du 9 au 14 octobre 2017, dans le cadre de la Saison numérique, la recherche prendra la forme d'une performance commentée. Après cinq journées de préparation, une print party sera jouée au studio de la Balsamine samedi 14 à 20h.

---

Up Pen Down is een onderzoek gebasseerd op de ontmoeting tussen digitale
typografie en choreografie. Door code als een daadwerkelijke beweging uit te
voeren onderzoeken we de lagunes tussen de praktijk can choregrafie en
programmeren. Na vijf dagen van voorbereiding ― open voor deelnemers, wordt het
atelier gepresenteerd met een lezing-performance open voor het publiek.


[Balsamine theater, Félix Marchallaan 1, 1030 Schaarbeek](http://balsamine.be/)








<style>
    h2 {
        color: red !important;
    }

    img.half {
        max-width 40%;
        display: inline-block;
    }
</style>
