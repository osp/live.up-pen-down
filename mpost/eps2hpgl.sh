#! /bin/bash

if [ "$#" -gt 0 ]; then
  eps=${1}
  base=$(basename ${eps} .1)
  dirty="${base}.dirty.hpgl"
  clean="${base}.hpgl"

  pstoedit -f plot-hpgl "${eps}" "${dirty}"
  # Clean HPGL
  cat "${dirty}" | python clean.py >  "${clean}"
  #rm "${dirty}"
else
  echo "Usage: eps2hpgl.sh [epsfile]"
  echo "Converts eps file to hpgl description"
fi
