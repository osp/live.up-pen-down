# % coordinates
# z1 =
# z2 =
# z3 =

points = [(0, 0), (2, 5), (4, 0), (1, 1), (3, 1)]

import random


def pickpoint(points):
    return 'p{0}'.format(random.randint(0, len(points) - 1))


def pickdirection():
    return random.choice(['left', 'right', 'up', 'down'])


mpost = 'draw {0}{{{1}}}'.format(pickpoint(points), pickdirection())

for step in range(random.randint(3, 10)):
    direction = pickdirection()
    target = pickpoint(points)

    mpost += '..{0}{{{1}}}'.format(pickpoint(points), pickdirection())

print """u := 1cm;
pair p[];
beginfig(1)"""
for key, point in enumerate(points):
    print "p{0} = ({1}u, {2}u);".format(key, point[0], point[1])
print mpost + ';'
print """for i=0 upto {0}: draw p[i] withpen pencircle scaled .15u;endfor;
endfig;
end;
""".format(len(points) - 1)
