from sys import stdin, stdout

allowed = ['PA', 'PR', 'PD', 'PU', 'SP']

filtered = ['SC700,1700,-700,1500']

for chunk in stdin.read().split(';'):
    if chunk[0:2] in allowed:
        filtered.append(chunk)

stdout.write(';'.join(filtered))
